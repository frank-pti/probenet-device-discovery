using Logging;
using Newtonsoft.Json;
using ProbeNet.Messages.Discovery;
/*
 * A C# library for discovering ProbeNet devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ProbeNet.DeviceDiscovery
{
    /// <summary>
    /// Discovery of ProbeNet devices.
    /// </summary>
    public class Discovery
    {
        /// <summary>
        /// Delegate method for measurement device discovered event.
        /// </summary>
        public delegate void MeasurementDeviceDiscoverdDelegate(Guid guid, IPEndPoint target);
        /// <summary>
        /// Occurs when measurement device discovered.
        /// </summary>
        public event MeasurementDeviceDiscoverdDelegate MeasurementDeviceDiscovered;

        private static List<IPAddress> LocalAddressList;

        private byte[] buffer;
        private Socket socket;
        private IPEndPoint localEndpoint;
        private EndPoint measurementDeviceEndpoint;
        private Random random;
        // private UdpClient sendClient;
        private List<UdpClient> sendClients;
        private byte[] requestData;
        private IPEndPoint multicastEndpoint;
        private InternetProtocol protocol;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.DeviceDiscovery.Discovery"/> class.
        /// </summary>
        public Discovery(InternetProtocol protocol)
        {
            this.sendClients = new List<UdpClient>();
            this.protocol = protocol;
            try {
                HasValidConnection = false;
                buffer = new byte[1024];
                random = new Random();

                switch (protocol) {
                    case InternetProtocol.V4:
                        socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                        break;
                    case InternetProtocol.V6:
                        socket = new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, ProtocolType.Udp);
                        break;
                }
                socket.MulticastLoopback = true;

                while (!socket.IsBound) {
                    try {
                        switch (protocol) {
                            case InternetProtocol.V4:
                                localEndpoint = new IPEndPoint(IPAddress.Any, BuildRandomPort());
                                break;
                            case InternetProtocol.V6:
                                localEndpoint = new IPEndPoint(IPAddress.IPv6Any, BuildRandomPort());
                                break;
                        }
                        socket.Bind(localEndpoint);
                    } catch (Exception) {
                        // nothing
                    }
                }
                Console.WriteLine("Listening for discovery on port {0}", localEndpoint.Port);

                Request request = new Request(localEndpoint.Port);
                requestData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(request));

                GetIpAddressList();

                foreach (IPAddress ip in LocalAddressList) {
                    if (ip.AddressFamily == AddressFamily.InterNetwork) {
                        UdpClient sendClient = null;
                        switch (protocol) {
                            case InternetProtocol.V4:
                                // sendClient = new UdpClient(BuildRandomPort(), AddressFamily.InterNetwork);
                                IPEndPoint ep = new IPEndPoint(ip, BuildRandomPort());
                                sendClient = new UdpClient(ep);
                                sendClient.MulticastLoopback = true;
                                multicastEndpoint = new IPEndPoint(IPAddress.Broadcast, 4001);
                                break;
                            case InternetProtocol.V6:
                                IPv6MulticastOption multicastOption = new IPv6MulticastOption(IPAddress.Parse("ff02::1"));
                                sendClient = new UdpClient(BuildRandomPort(), AddressFamily.InterNetworkV6);
                                sendClient.MulticastLoopback = true;
                                sendClient.JoinMulticastGroup(multicastOption.Group);
                                multicastEndpoint = new IPEndPoint(multicastOption.Group, 4001);
                                break;
                        }
                        if (sendClient != null) {
                            sendClients.Add(sendClient);
                        }
                    }
                }

                BeginReceiveAnswer();
                HasValidConnection = true;
            } catch (Exception e) {
                Logger.Log(e, "Received error while checking for ProbeNet devices");
            }
        }

        internal static List<IPAddress> GetIpAddressList()
        {
            if (LocalAddressList == null) {
                IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                LocalAddressList = new List<IPAddress>();
                Logger.Log("Host '{0}'", Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList) {
                    if (ip.AddressFamily == AddressFamily.InterNetwork) {
                        Logger.Log(" IP Address: {0}", ip.ToString());
                        LocalAddressList.Add(ip);
                    }
                }
            }
            return LocalAddressList;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has valid connection.
        /// </summary>
        /// <value><c>true</c> if this instance has valid connection; otherwise, <c>false</c>.</value>
        public bool HasValidConnection
        {
            get;
            private set;
        }

        private void BeginReceiveAnswer()
        {
            switch (protocol) {
                case InternetProtocol.V4:
                    measurementDeviceEndpoint = new IPEndPoint(IPAddress.Any, 0);
                    break;
                case InternetProtocol.V6:
                    measurementDeviceEndpoint = new IPEndPoint(IPAddress.IPv6Any, 0);
                    break;
            }
            socket.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref measurementDeviceEndpoint,
                AnswerReceived, null);
        }

        private void AnswerReceived(IAsyncResult asyncResult)
        {
            int count = socket.EndReceiveFrom(asyncResult, ref measurementDeviceEndpoint);
            string answerString = Encoding.UTF8.GetString(buffer, 0, count);
            Logger.Log("Answer received: {0} bytes: {1}", count, answerString);
            try {
                Answer answer = JsonConvert.DeserializeObject<Answer>(answerString);
                if (measurementDeviceEndpoint is IPEndPoint) {
                    IPEndPoint ipEndpoint = measurementDeviceEndpoint as IPEndPoint;
                    IPEndPoint target = new IPEndPoint(ipEndpoint.Address, answer.Port);
                    NotifyDiscoveryAnswerReceived(answer.Uuid, target);
                }
            } catch (Exception) {
                // ignore everything we do not understand
            }
            BeginReceiveAnswer();
        }

        private int BuildRandomPort()
        {
            return random.Next(49152, 65535);
        }

        /// <summary>
        /// Discover the device.
        /// </summary>
        public void Discover()
        {
            try {
                foreach (UdpClient sendClient in sendClients) {
                    sendClient.Send(requestData, requestData.Length, multicastEndpoint);
                }
            } catch (Exception e) {
                Logger.Log(e);
                HasValidConnection = false;
            }
        }

        private void NotifyDiscoveryAnswerReceived(Guid guid, IPEndPoint target)
        {
            if (MeasurementDeviceDiscovered != null) {
                MeasurementDeviceDiscovered(guid, target);
            }
        }
    }
}
