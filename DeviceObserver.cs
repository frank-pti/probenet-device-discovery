/*
 * A C# library for discovering ProbeNet devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using FrankPti.Collections;
using Logging;
using ProbeNet.Communication;
using ProbeNet.Messages;
using ProbeNet.Messages.Raw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace ProbeNet.DeviceDiscovery
{
    /// <summary>
    /// Class for observing devices.
    /// </summary>
    public class DeviceObserver
    {
        /// <summary>Delegate method for device description host events.</summary>
        public delegate void DeviceDescriptionHostDelegate(Guid guid, string address, int port);
        /// <summary>Delegate method for device description eents.</summary>
        public delegate void DeviceDescriptionDelegate(Guid guid);
        /// <summary>
        /// Occurs when device added.
        /// </summary>
        public event DeviceDescriptionHostDelegate DeviceAdded;
        /// <summary>
        /// Occurs when device removed.
        /// </summary>
        public event DeviceDescriptionDelegate DeviceRemoved;

        private string language;
        private Dictionary<Guid, ProbeNetChannel> probeNetChannels;
        private Dictionary<Guid, Dictionary<string, DeviceDescription>> deviceDescriptions;
        private Dictionary<Guid, Tuple<string, int>> addresses;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.DeviceDiscovery.DeviceObserver"/> class.
        /// </summary>
        /// <param name="language">Language.</param>
        /// <param name="finder">Finder.</param>
        public DeviceObserver(string language, DeviceFinder finder)
        {
            this.language = language;
            probeNetChannels = new Dictionary<Guid, ProbeNetChannel>();
            deviceDescriptions = new Dictionary<Guid, Dictionary<string, DeviceDescription>>();
            addresses = new Dictionary<Guid, Tuple<string, int>>();
            finder.DeviceAppeared += HandleDeviceAppeared;
            finder.DeviceVanished += HandleDeviceVanished;
            LoadDevicesInitially(finder);
        }

        private void LoadDevicesInitially(DeviceFinder finder)
        {
            foreach (KeyValuePair<Guid, IPEndPoint> entry in finder.Devices) {
                QueryDevice(entry.Key, entry.Value);
            }
        }

        private void HandleDeviceAppeared(Guid guid, IPEndPoint target)
        {
            Console.WriteLine("Device appeared: {0}", guid);
            QueryDevice(guid, target);
        }

        private void QueryDevice(Guid guid, IPEndPoint target)
        {
            try {
                string address = target.Address.ToString();
                int port = target.Port;
                Socket socket = new Socket(target.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(target);
                ProbeNetChannel channel = new ProbeNetChannel(new NetworkStream(socket), String.Format("TCP {0}", target));
                channel.MessageReceived += (chan, msg) => {
                    HandleMessageReceived(chan, msg, address, port);
                };
                channel.Send(new Message(new MessageHeader(language), new DeviceDescriptionRequest()));
            } catch (Exception e) {
                Logger.Log(e, "Could not connect to device with guid {0}, endpoint {1}", guid, target);
            }
        }

        private Dictionary<Tuple<ProbeNetChannel, string, int>, Dictionary<string, DeviceDescription>> incomingDevices;

        private void HandleMessageReceived(ProbeNetChannel channel, Message message, string address, int port)
        {
            if (incomingDevices == null) {
                incomingDevices = new Dictionary<Tuple<ProbeNetChannel, string, int>, Dictionary<string, DeviceDescription>>();
            }
            DeviceDescription deviceDescription = message.Body as DeviceDescription;
            string language = message.Header.Language;
            Tuple<ProbeNetChannel, string, int> key = Tuple.Create(channel, address, port);

            if (message.Body is DeviceDescription) {
                ProcessDeviceDescription(deviceDescription, language, key);
            } else if (message.Body is MeasurementDescription) {
                MeasurementDescription measurementDescription = message.Body as MeasurementDescription;
                IDictionary<string, DeviceDescription> incomingDevice = incomingDevices[key];
                int index = int.Parse(message.Header.Tag);
                if (incomingDevice.ContainsKey(language)) {
                    incomingDevice[language].Measurements[index] = measurementDescription;
                } else {
                    Logger.Error("No device description for measurement description '{0}' found for language {1}",
                        measurementDescription.Id, language);
                }

            } else if (message.Body is SequenceDescription) {
                SequenceDescription sequenceDescription = message.Body as SequenceDescription;
                IDictionary<string, DeviceDescription> incomingDevice = incomingDevices[key];
                int index = int.Parse(message.Header.Tag);
                if (incomingDevice.ContainsKey(language)) {
                    incomingDevice[language].Sequences[index] = sequenceDescription;
                } else {
                    Logger.Error("No device description for sequence description '{0}' found for language {1}",
                        sequenceDescription.Id, language);
                }
            } else {
                return;
            }
            lock (incomingDevices) {
                foreach (Tuple<ProbeNetChannel, string, int> tuple in incomingDevices.Keys) {
                    Dictionary<string, DeviceDescription> val = incomingDevices[tuple];
                    if (val.Values.Count == val.First().Value.InstalledLanguages.Count &&
                        CountEmptyMeasurementDescription(val) == 0 &&
                        CountEmptySequenceDescription(val) == 0) {
                        AddDevice(val, tuple.Item1, tuple.Item2, tuple.Item3);
                    }
                }
            }
        }

        private void ProcessDeviceDescription(
            DeviceDescription deviceDescription, string language, Tuple<ProbeNetChannel, string, int> key)
        {
            int emptyMeasurementDescriptionCount = CountEmptyMeasurementDescription(deviceDescription);
            int emptySequenceDescriptionCount = CountEmptySequenceDescription(deviceDescription);

            if (emptySequenceDescriptionCount == 0 && emptyMeasurementDescriptionCount == 0) {
                if (incomingDevices.ContainsKey(key)) {
                    Dictionary<string, DeviceDescription> descriptions = incomingDevices[key];
                    if (descriptions.ContainsKey(language)) {
                        descriptions[language] = deviceDescription;
                    } else {
                        descriptions.Add(language, deviceDescription);
                    }
                    incomingDevices[key] = descriptions;
                } else {
                    incomingDevices.Add(key, BuildNewDeviceDescriptions(language, deviceDescription));
                }
            } else {
                if (emptyMeasurementDescriptionCount > 0) {
                    // Measurement descriptions did not come with device description - query separately
                    if (incomingDevices.ContainsKey(key)) {
                        Dictionary<string, DeviceDescription> deviceDescriptions = incomingDevices[key];
                        if (deviceDescriptions.ContainsKey(language)) {
                            deviceDescriptions[language] = deviceDescription;
                        } else {
                            deviceDescriptions.Add(language, deviceDescription);
                        }
                        incomingDevices[key] = deviceDescriptions;
                    } else {
                        incomingDevices.Add(key, BuildNewDeviceDescriptions(language, deviceDescription));
                    }
                    for (int i = 0; i < deviceDescription.Measurements.Count; i++) {
                        Message m = new Message(
                            new MessageHeader(language, DateTime.Now, String.Format("{0}", i)),
                            new MeasurementDescriptionRequest(i));
                        key.Item1.Send(m);
                    }
                }
                if (emptySequenceDescriptionCount > 0) {
                    for (int i = 0; i < deviceDescription.Sequences.Count; i++) {
                        Message m = new Message(
                            new MessageHeader(language, DateTime.Now, String.Format("{0}", i)),
                            new SequenceDescriptionRequest(i));
                        key.Item1.Send(m);
                    }
                }
            }

            foreach (string lang in deviceDescription.InstalledLanguages) {
                // request other languages
                IDictionary<string, DeviceDescription> receivedDescriptions = incomingDevices[key];
                if (!incomingDevices.ContainsKey(key) || !incomingDevices[key].ContainsKey(lang)) {
                    Message m = new Message(
                        new MessageHeader(lang, DateTime.Now, null),
                        new DeviceDescriptionRequest());
                    key.Item1.Send(m);
                }
            }
        }

        public static int CountEmptyMeasurementDescription(IDictionary<string, DeviceDescription> deviceDescriptions)
        {
            int sum = 0;
            foreach (DeviceDescription deviceDescription in deviceDescriptions.Values) {
                sum += CountEmptyMeasurementDescription(deviceDescription);
            }
            return sum;
        }

        public static int CountEmptyMeasurementDescription(DeviceDescription deviceDescription)
        {
            return deviceDescription.Measurements.Count(measurementDescription => measurementDescription == null);
        }

        private static int CountEmptySequenceDescription(IDictionary<string, DeviceDescription> deviceDescriptions)
        {
            int sum = 0;
            foreach (DeviceDescription deviceDescription in deviceDescriptions.Values) {
                sum += CountEmptySequenceDescription(deviceDescription);
            }
            return sum;
        }

        public static int CountEmptySequenceDescription(DeviceDescription deviceDescription)
        {
            return deviceDescription.Sequences.Count(sequencesDescription => sequencesDescription == null);
        }

        private static Dictionary<string, DeviceDescription> BuildNewDeviceDescriptions(
            string language, DeviceDescription deviceDescription)
        {
            return new Dictionary<string, DeviceDescription>() {
                { language, deviceDescription }
            };
        }

        private void HandleDeviceVanished(Guid guid)
        {
            RemoveDevice(guid);
        }

        private void AddDevice(Dictionary<string, DeviceDescription> deviceDescriptions, ProbeNetChannel channel, string address, int port)
        {
            DeviceDescription deviceDescription = deviceDescriptions.First().Value;
            if (probeNetChannels.ContainsKey(deviceDescription.Uuid)) {
                RemoveDevice(deviceDescription.Uuid);
            }
            this.probeNetChannels.Add(deviceDescription.Uuid, channel);
            this.deviceDescriptions.Add(deviceDescription.Uuid, deviceDescriptions);
            this.addresses.Add(deviceDescription.Uuid, new Tuple<string, int>(address, port));
            if (DeviceAdded != null) {
                DeviceAdded(deviceDescription.Uuid, address, port);
            }
        }

        private void RemoveDevice(Guid guid)
        {
            probeNetChannels[guid].Stop();
            probeNetChannels.Remove(guid);
            deviceDescriptions.Remove(guid);
            if (DeviceRemoved != null) {
                DeviceRemoved(guid);
            }
        }

        /// <summary>
        /// Gets the devices.
        /// </summary>
        /// <value>The devices.</value>
        public IDictionary<Guid, Dictionary<string, DeviceDescription>> Devices
        {
            get
            {
                return new ImmutableDictionary<Guid, Dictionary<string, DeviceDescription>>(deviceDescriptions);
            }
        }

        /// <summary>
        /// Gets the addresses.
        /// </summary>
        /// <value>The addresses.</value>
        public IDictionary<Guid, Tuple<string, int>> Addresses
        {
            get
            {
                return new ImmutableDictionary<Guid, Tuple<string, int>>(addresses);
            }
        }

        /// <summary>
        /// Gets the channels.
        /// </summary>
        /// <value>The channels.</value>
        public IDictionary<Guid, ProbeNetChannel> Channels
        {
            get
            {
                return new ImmutableDictionary<Guid, ProbeNetChannel>(probeNetChannels);
            }
        }
    }
}

