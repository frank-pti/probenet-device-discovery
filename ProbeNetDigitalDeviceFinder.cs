﻿using Logging;
using Newtonsoft.Json;
using ProbeNet.Communication;
using ProbeNet.Messages;
using ProbeNet.Messages.Raw;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ProbeNet.DeviceDiscovery
{
    public class ProbeNetDigitalDeviceFinder
    {
        public delegate void DeviceDescriptionReceivedDelegate(string language, string address, int port, DeviceDescription deviceDescription);
        public event DeviceDescriptionReceivedDelegate DeviceDescriptionReceived;

        public delegate void SequenceDescriptionReceivedDelegate(string language, int index, SequenceDescription sequenceDescription);
        public event SequenceDescriptionReceivedDelegate SequenceDescriptionReceived;

        public delegate void MeasurementDescriptionReceivedDelegate(string language, int index, MeasurementDescription measurementDescription);
        public event MeasurementDescriptionReceivedDelegate MeasurementDescriptionReceived;

        private const int listenPort = 11001;
        private const int dataPort = 11000;

        private static UdpClient client;
        private static bool running;

        private Thread thread;
        private Socket socket;
        private FrameParser frameParser;
        private Queue<byte> queue;
        private Dictionary<string, DeviceDescription> currentDeviceDescription;
        private bool reading;

        private static ProbeNetDigitalDeviceFinder instance = null;

        public static ProbeNetDigitalDeviceFinder GetInstance()
        {
            if (instance == null) {
                instance = new ProbeNetDigitalDeviceFinder();
            }
            return instance;
        }

        private ProbeNetDigitalDeviceFinder()
        {
            queue = new Queue<byte>();
            currentDeviceDescription = new Dictionary<string, DeviceDescription>();

            frameParser = new FrameParser();
            frameParser.FrameParsedString += FrameParserParsedString;
            frameParser.FrameError += FrameParserError;

            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            socket.EnableBroadcast = true;
            socket.MulticastLoopback = true;
            running = true;
            reading = false;
            Start();
        }

        public void Start()
        {
            thread = new Thread(StartServer) {
                Name = "ProbeNet Digital Device Finder",
                IsBackground = true
            };
            thread.Start();
        }

        public void Stop()
        {
            //running = false;
        }

        private void StartServer()
        {
            running = true;
            if (client == null) {
                client = new UdpClient(listenPort);
                client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                client.EnableBroadcast = true;
            }
            while (true) {
                //HINT: If nothing is received, probably a firewall is blocking incoming data!
                IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = client.Receive(ref remote);
                if (running) {
                    HandleProbeNetDigitalAnswer(data, 0, data.Length);
                } else {
                    Logger.Log("ProbeNet digital discovery data (length {0}) ignored.", data.Length);
                }
            }
            Logger.Log("ProbeNet Digital Device Finder stopped.");
        }

        private void HandleProbeNetDigitalAnswer(byte[] buffer, int offset, int count)
        {
            /*Logger.Log("Parsing ProbeNet digital discovery data from {0}, count {1} in '{2}'",
                offset, count, System.Text.Encoding.UTF8.GetString(buffer));*/
            for (int i = offset; i < offset + count; i++) {
                byte b = buffer[i];
                if (b == FrameDefinitions.START_BYTE) {
                    queue.Clear();
                    reading = true;
                }
                if (reading) {
                    queue.Enqueue(b);
                }
                if (b == FrameDefinitions.END_BYTE) {
                    reading = false;
                    byte[] data = queue.ToArray();
                    if (data.Length >= 2) {
                        byte[] frame = FrameEncoder.Encode(data, 1, data.Length - 2);
                        frameParser.Parse(frame, 0, frame.Length);
                    } else {
                        Logger.Log("ProbeNet Digital Device Finder: Only {0} bytes found between STX and ETX.", data.Length);
                    }
                }
            }
        }

        private void FrameParserParsedString(string data)
        {
            Message message = MessageDecoder.Decode(data);
            string language = message.Header.Language;
            ProbeNetDigitalTag tag = message.Header.Tag != null ?
                JsonConvert.DeserializeObject<ProbeNetDigitalTag>(message.Header.Tag) : null;
            if (message.Body is DeviceDescription) {
                if (currentDeviceDescription.ContainsKey(language)) {
                    currentDeviceDescription[language] = message.Body as DeviceDescription;
                } else {
                    currentDeviceDescription.Add(language, message.Body as DeviceDescription);
                }
            } else if (message.Body is SequenceDescription) {
                int index = tag.Index;
                if (currentDeviceDescription.ContainsKey(language) && currentDeviceDescription[language].Uuid.Equals(tag.Uuid)) {
                    currentDeviceDescription[language].Sequences[index] = message.Body as SequenceDescription;
                }
            } else if (message.Body is MeasurementDescription) {
                int index = tag.Index;
                if (currentDeviceDescription.ContainsKey(language) && currentDeviceDescription[language].Uuid.Equals(tag.Uuid)) {
                    currentDeviceDescription[language].Measurements[index] = message.Body as MeasurementDescription;
                }
            } else {
                Logging.Logger.Log("Cannot handle message '{0}' here", message.Body.Type);
            }
            foreach (KeyValuePair<string, DeviceDescription> description in currentDeviceDescription) {
                int emptyMeasurementDescriptions = DeviceObserver.CountEmptyMeasurementDescription(description.Value);
                int emptySequenceDescriptions = DeviceObserver.CountEmptySequenceDescription(description.Value);
                if (emptySequenceDescriptions == 0 && emptyMeasurementDescriptions == 0) {
                    NotifyDeviceDescriptionReceived(description.Key, null, dataPort, description.Value);
                }
            }
        }

        private void FrameParserError(string error)
        {
            Logging.Logger.Log("Error parsing frame received by UDP: {0}", error);
        }

        private void NotifyDeviceDescriptionReceived(string language, string address, int port, DeviceDescription deviceDescription)
        {
            if (DeviceDescriptionReceived != null) {
                DeviceDescriptionReceived(language, address, port, deviceDescription);
            }
        }
    }
}
