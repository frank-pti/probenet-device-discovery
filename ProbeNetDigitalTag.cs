﻿using Newtonsoft.Json;
using System;

namespace ProbeNet.DeviceDiscovery
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ProbeNetDigitalTag
    {
        public ProbeNetDigitalTag()
        {
            Uuid = new Guid();
            Index = -1;
        }

        [JsonProperty("uuid")]
        public Guid Uuid
        {
            get;
            set;
        }

        [JsonProperty("index")]
        public int Index
        {
            get;
            set;
        }
    }
}
