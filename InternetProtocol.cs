﻿
namespace ProbeNet.DeviceDiscovery
{
    /// <summary>
    /// List of supported Internet protocols (IP) versions for discovery.
    /// </summary>
    public enum InternetProtocol
    {
        /// <summary>
        /// Internet protocol version 4 (IP, IPv4)
        /// </summary>
        V4,

        /// <summary>
        /// Internet protocol version 6 (IPv6)
        /// </summary>
        V6
    }
}
