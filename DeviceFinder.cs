using FrankPti.Collections;
/*
 * A C# library for discovering ProbeNet devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Net;
using System.Timers;

namespace ProbeNet.DeviceDiscovery
{
    /// <summary>
    /// Class for finding ProbeNet devices.
    /// </summary>
    public class DeviceFinder
    {
        /// <summary>
        /// Delegate method for device appeared event.
        /// </summary>
        public delegate void DeviceAppearedDelegate(Guid guid, IPEndPoint target);
        /// <summary>
        /// Occurs when device appeared.
        /// </summary>
        public event DeviceAppearedDelegate DeviceAppeared;

        /// <summary>
        /// Delegate method for device vanished event.
        /// </summary>
        public delegate void DeviceVanishedDelegate(Guid guid);
        /// <summary>
        /// Occurs when device vanished.
        /// </summary>
        public event DeviceVanishedDelegate DeviceVanished;

        private Discovery discovery;
        private Dictionary<Guid, IPEndPoint> devices;
        private List<Guid> toBeRemoved;

        private Timer timer;
        private int timerCounter;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.DeviceDiscovery.DeviceFinder"/> class.
        /// </summary>
        /// <param name="discovery">Discovery.</param>
        public DeviceFinder(Discovery discovery)
        {
            devices = new Dictionary<Guid, IPEndPoint>();
            toBeRemoved = new List<Guid>(devices.Keys);

            this.discovery = discovery;
            this.discovery.MeasurementDeviceDiscovered += HandleDiscoveryhandleMeasurementDeviceDiscovered;

            timerCounter = 0;

            timer = new Timer(1000);
            timer.Elapsed += HandleTimerElapsed;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this device finder is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled
        {
            get
            {
                return timer.Enabled;
            }
            set
            {
                if (timer.Enabled != value) {
                    if (value) {
                        this.discovery.Discover();
                    }
                    if (this.discovery.HasValidConnection || !value) {
                        timer.Enabled = value;
                    }
                }
            }
        }

        private void HandleTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (timerCounter == 5) {
                CleanupDevices();
                timerCounter = 0;
            } else {
                timerCounter++;
            }
            discovery.Discover();
        }

        private void CleanupDevices()
        {
            foreach (Guid guid in toBeRemoved) {
                devices.Remove(guid);
                NotifyDeviceVanished(guid);
            }
            toBeRemoved = new List<Guid>(devices.Keys);
        }

        private void HandleDiscoveryhandleMeasurementDeviceDiscovered(Guid guid, IPEndPoint target)
        {
            if (toBeRemoved.Contains(guid)) {
                toBeRemoved.Remove(guid);
            }
            if (!devices.ContainsKey(guid)) {
                devices.Add(guid, target);
                NotifyDeviceAppeared(guid, target);
            }
        }

        private void NotifyDeviceAppeared(Guid guid, IPEndPoint target)
        {
            if (DeviceAppeared != null) {
                DeviceAppeared(guid, target);
            }
        }

        private void NotifyDeviceVanished(Guid guid)
        {
            if (DeviceVanished != null) {
                DeviceVanished(guid);
            }
        }

        /// <summary>
        /// Gets the devices.
        /// </summary>
        /// <value>The devices.</value>
        public IDictionary<Guid, IPEndPoint> Devices
        {
            get
            {
                return new ImmutableDictionary<Guid, IPEndPoint>(devices);
            }
        }
    }
}